package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class Login {

	@When("User input value of email on login page with {string}")
	public void user_input_value_of_email_on_login_page_with(String email) {
		Mobile.setText(findTestObject('Page_Login/Form_Email'), email , 0)
	}

	@When("User input value of password on login page with {string}")
	public void user_input_value_of_password_on_login_page_with(String password) {
		Mobile.setText(findTestObject('Page_Register/Form_Password'), password , 0)
	}

	@When("User click button login")
	public void user_click_button_login() {
		Mobile.tap(findTestObject('Page_Login/Button_Login'), 0)
	}

	@Then("User login successfully")
	public void user_login_successfully() {
		Mobile.verifyElementVisible(findTestObject('Page_Homepage/Text_Android New'), 0)
		Mobile.delay(5)
		Mobile.closeApplication()
	}
}
