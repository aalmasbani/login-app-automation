package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
public class RandomEmail {
	@Keyword
	public String getEmail(String suffix,String prefix){
		int randomNo = (int)(Math.random() * 1000);
		return suffix + randomNo + "@" + prefix;
	}
}
public class Regitration {
	@Given("User open Login Apps")
	public void user_open_Login_Apps() {
		Mobile.startApplication('APK File\\Sample Android App Login Test.apk', false)
	}

	@When("User click button register on login page")
	public void user_click_button_register_on_login_page() {
		Mobile.tap(findTestObject('Page_Login/Button_Register'), 0)
	}

	@When("User input value of name with {string}")
	public void user_input_value_of_name_with(String name) {
		Mobile.setText(findTestObject('Page_Register/Form_Name'), name , 0)
	}

	@When("User input value of email with {string}")
	public void user_input_value_of_email_with(String email) {
		Mobile.setText(findTestObject('Page_Register/Form_Email'), email , 0)
	}

	@When("User input value of password with {string}")
	public void user_input_value_of_password_with(String password) {
		Mobile.setText(findTestObject('Page_Register/Form_Password'), password , 0)
	}

	@When("User input value of confirm password with {string}")
	public void user_input_value_of_confirm_password_with(String password) {
		Mobile.setText(findTestObject('Page_Register/Form_Konfirmasi Password'), password , 0)
	}

	@When("User click button register")
	public void user_click_button_register() {
		Mobile.tap(findTestObject('Page_Register/Button_Register'), 0)
	}

	@Then("Account registered successfully")
	public void account_registered_successfully() {
		Mobile.tap(findTestObject('Page_Register/Text_Registration Successful'), 0)
		Mobile.delay(5)
	}
}



